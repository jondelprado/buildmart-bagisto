<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Webkul\Category\Models\Category;
use Webkul\Category\Models\CategoryTranslation;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class CategoryController extends Controller
{


  public function categoryList(){

    $has_subcategory = 0;
    $category_id_arr = [];

    $categories = Category::where([
      // ['parent_id', '=', 1],
      // ['position', '=', 1],
      ['parent_id', '!=', null],
      ])->get();

      foreach ($categories as $category) {
        array_push($category_id_arr, $category->id);
      }

      $category_details = CategoryTranslation::whereIn('category_id', $category_id_arr)->where('locale', 'en')->get();

    return response()->json(array(
      "categories" => $categories,
      "category_details" => $category_details,
    ));

  }

}
