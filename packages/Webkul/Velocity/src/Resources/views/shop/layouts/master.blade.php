@php
    $velocityHelper = app('Webkul\Velocity\Helpers\Helper');
    $velocityMetaData = $velocityHelper->getVelocityMetaData();

    view()->share('velocityMetaData', $velocityMetaData);
@endphp

<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

    <head>
        <title>@yield('page_title')</title>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta http-equiv="content-language" content="{{ app()->getLocale() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="stylesheet" href="{{ asset('themes/velocity/assets/css/velocity.css') }}" />
        <link rel="stylesheet" href="{{ asset('themes/velocity/assets/css/bootstrap.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('themes/velocity/assets/css/google-font.css') }}" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" crossorigin="anonymous" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.3/css/bootstrap.min.css" crossorigin="anonymous" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css" crossorigin="anonymous" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css" crossorigin="anonymous" />
        <style media="screen">
          #top, #nav_container .row, #cart_container {
            height: 100px!important;
          }
          
          #top {
            background-image: linear-gradient(90deg, rgba(255, 133, 0, 1) 10%, rgba(255, 145, 25, 1) 20%, rgba(255, 157, 51, 1) 30%);
            border: none;
          }

          #logo_container img{
            object-fit: contain;
          }

          .menu_item {
            margin:  auto;
            padding: 0;
          }

          .cart_item {
            background-color: #f2f2f2;
          }

          .menu_item a, .cart_item a {
            font-size:   16px;
            font-weight: 600;
          }

          .menu_item a {
            color: #f2f2f2;
          }

          .cart_item a {
            color:      #ff8500;
            position:   absolute;
            top:        50%;
            left:       50%;
            transform:  translate(-50%, -50%);
            text-align: center;
          }

          .carousel-item img{
            display: flex;
          }

          .carousel-item img{
            height:     333px;
            width:      33%;
            object-fit: cover;
            display:    inline-block;
          }
          
          .category_nav {
            padding: .5rem 1rem;
          }

          .category_nav .row{
            height: 100px;
          }

          .category_nav a {
            color:       #2f2f2f;
            font-weight: 500px;
            position:    absolute;
            top:         50%;
            left:        50%;
            transform:   translate(-50%, -50%);
          }

          .category_container {
            padding: 3em;
          }

          .category_item {
            height: 200px;
          }

          .category_item img {
            display:      inline-block;
            height:       50px;
            width:        50px;
            margin-right: 1em;
          }

          .category_item a {
            color:          #ff8500;
            text-transform: uppercase;
            font-size:      20px;
            font-weight:    600;
          }

          .slick-prev:before, .slick-next:before {
            color: #ff8500;
          }

          .theme-btn, .btn-add-to-cart, .theme-btn.remove-decoration {
            background-color: #FF8500!important;
          }

          .theme-btn:hover, .btn-add-to-cart:hover, .theme-btn:active, .btn-add-to-cart:active {
            background-color: #FF8500!important;
            border-color:     #FF8500!important;
            opacity:          1!important;
          }
          
          .content-list ul {
            background-color: none!important;
            background-image: linear-gradient(90deg, rgba(255, 133, 0, 1) 10%, rgba(255, 145, 25, 1) 20%, rgba(255, 157, 51, 1) 30%);
          }

          .sticky-header, .sticky-header .mini-cart-content, .sticky-header .down-arrow-container {
            color: #f2f2f2!important;
          }
        </style>

        @if (core()->getCurrentLocale()->direction == 'rtl')
            <link href="{{ asset('themes/velocity/assets/css/bootstrap-flipped.css') }}" rel="stylesheet">
        @endif

        @if ($favicon = core()->getCurrentChannel()->favicon_url)
            <link rel="icon" sizes="16x16" href="{{ $favicon }}" />
        @else
            <link rel="icon" sizes="16x16" href="{{ asset('/themes/velocity/assets/images/static/v-icon.png') }}" />
        @endif

        <script
            type="text/javascript"
            baseUrl="{{ url()->to('/') }}"
            src="{{ asset('themes/velocity/assets/js/velocity.js') }}">
        </script>

        <script
            type="text/javascript"
            src="{{ asset('themes/velocity/assets/js/jquery.ez-plus.js') }}">
        </script>

        <script
            type="text/javascript"
            src="{{ asset('themes/velocity/assets/js/jquery.min.js') }}">
        </script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.3/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>

        @yield('head')

        @section('seo')
            <meta name="description" content="{{ core()->getCurrentChannel()->description }}"/>
        @show

        @stack('css')

        {!! view_render_event('bagisto.shop.layout.head') !!}

        <style>
            {!! core()->getConfigData('general.content.custom_scripts.custom_css') !!}
        </style>

    </head>

    <body @if (core()->getCurrentLocale()->direction == 'rtl') class="rtl" @endif>
        {!! view_render_event('bagisto.shop.layout.body.before') !!}

        @include('shop::UI.particals')

        <div id="app">
            {{-- <responsive-sidebar v-html="responsiveSidebarTemplate"></responsive-sidebar> --}}

            <product-quick-view v-if="$root.quickView"></product-quick-view>

            <div class="main-container-wrapper">

                @section('body-header')
                    @include('shop::layouts.top-nav.index')

                    {!! view_render_event('bagisto.shop.layout.header.before') !!}

                        @include('shop::layouts.header.index')

                    {!! view_render_event('bagisto.shop.layout.header.after') !!}

                    <div class="main-content-wrapper col-12 no-padding">

                      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                          <div class="carousel-item active">
                            <img src="{{asset('images/BUILDMART-3.jpg')}}">
                            <img src="{{asset('images/BUILDMART-6.jpg')}}">
                            <img src="{{asset('images/BUILDMART-9.jpg')}}">
                          </div>
                          <div class="carousel-item">
                            <img src="{{asset('images/BUILDMART-13.jpg')}}">
                            <img src="{{asset('images/BUILDMART-16.jpg')}}">
                            <img src="{{asset('images/BUILDMART-19.jpg')}}">
                          </div>
                          <div class="carousel-item">
                            <img src="{{asset('images/BUILDMART-23.jpg')}}">
                            <img src="{{asset('images/BUILDMART-26.jpg')}}">
                            <img src="{{asset('images/BUILDMART-29.jpg')}}">
                          </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                          <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                          <span class="carousel-control-next-icon" aria-hidden="true"></span>
                          <span class="sr-only">Next</span>
                        </a>
                      </div>

                      <!-- <div class="category_nav">
                        <div class="row">
                          <div class="col-sm">
                            <a href="#">All</a>
                          </div>
                          <div class="col-sm">
                            <a href="#">Electrical</a>
                          </div>
                          <div class="col-sm">
                            <a href="#">Toilets and Faucets</a>
                          </div>
                          <div class="col-sm">
                            <a href="#">Building Materials</a>
                          </div>
                          <div class="col-sm">
                            <a href="#">Doors and Windows</a>
                          </div>
                          <div class="col-sm">
                            <a href="#">Tiles and Vinyls</a>
                          </div>
                          <div class="col-sm">
                            <a href="#">Hardware</a>
                          </div>
                          <div class="col-sm">
                            <a href="#">Paint</a>
                          </div>
                          <div class="col-sm">
                            <a href="#">Plumbing</a>
                          </div>
                          <div class="col-sm">
                            <a href="#">Tools</a>
                          </div>
                        </div>
                      </div> -->

                      <!-- <div class="category_container">
                        <div class="category_list text-center">
                          <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              Dropdown button
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                              <a class="dropdown-item" href="#">Action</a>
                              <a class="dropdown-item" href="#">Another action</a>
                              <a class="dropdown-item" href="#">Something else here</a>
                            </div>
                          </div>
                        </div>
                      </div> -->



                        @php
                            $velocityContent = app('Webkul\Velocity\Repositories\ContentRepository')->getAllContents();
                        @endphp

                        <content-header
                            url="{{ url()->to('/') }}"
                            :header-content="{{ json_encode($velocityContent) }}"
                            heading= "{{ __('velocity::app.menu-navbar.text-category') }}"
                            category-count="{{ $velocityMetaData ? $velocityMetaData->sidebar_category_count : 10 }}"
                        ></content-header>
                        <div class="">
                            <div class="row col-12 remove-padding-margin">
                                <sidebar-component
                                    main-sidebar=true
                                    id="sidebar-level-0"
                                    url="{{ url()->to('/') }}"
                                    category-count="{{ $velocityMetaData ? $velocityMetaData->sidebar_category_count : 10 }}"
                                    add-class="category-list-container pt10">
                                </sidebar-component>
<!-- 
                                <div
                                    class="col-12 no-padding content" id="home-right-bar-container">

                                    <div class="container-right row no-margin col-12 no-padding">

                                        {!! view_render_event('bagisto.shop.layout.content.before') !!}

                                        @yield('content-wrapper')

                                        {!! view_render_event('bagisto.shop.layout.content.after') !!}
                                    </div>

                                </div> -->

                            </div>
                        </div>

                    </div>
                @show

                <div class="container">

                    {!! view_render_event('bagisto.shop.layout.full-content.before') !!}

                        @yield('full-content-wrapper')

                    {!! view_render_event('bagisto.shop.layout.full-content.after') !!}

                </div>
            </div>

            <div class="modal-parent" id="loader" style="top: 0" v-show="showPageLoader">
                <overlay-loader :is-open="true"></overlay-loader>
            </div>
        </div>

        <!-- below footer -->
        @section('footer')
            {!! view_render_event('bagisto.shop.layout.footer.before') !!}

                @include('shop::layouts.footer.index')

            {!! view_render_event('bagisto.shop.layout.footer.after') !!}
        @show

        {!! view_render_event('bagisto.shop.layout.body.after') !!}

        <div id="alert-container"></div>

        <script type="text/javascript">
            (() => {
                window.showAlert = (messageType, messageLabel, message) => {
                    if (messageType && message !== '') {
                        let alertId = Math.floor(Math.random() * 1000);

                        let html = `<div class="alert ${messageType} alert-dismissible" id="${alertId}">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>${messageLabel ? messageLabel + '!' : ''} </strong> ${message}.
                        </div>`;

                        $('#alert-container').append(html).ready(() => {
                            window.setTimeout(() => {
                                $(`#alert-container #${alertId}`).remove();
                            }, 5000);
                        });
                    }
                }

                let messageType = '';
                let messageLabel = '';

                @if ($message = session('success'))
                    messageType = 'alert-success';
                    messageLabel = "{{ __('velocity::app.shop.general.alert.success') }}";
                @elseif ($message = session('warning'))
                    messageType = 'alert-warning';
                    messageLabel = "{{ __('velocity::app.shop.general.alert.warning') }}";
                @elseif ($message = session('error'))
                    messageType = 'alert-danger';
                    messageLabel = "{{ __('velocity::app.shop.general.alert.error') }}";
                @elseif ($message = session('info'))
                    messageType = 'alert-info';
                    messageLabel = "{{ __('velocity::app.shop.general.alert.info') }}";
                @endif

                if (messageType && '{{ $message }}' !== '') {
                    window.showAlert(messageType, messageLabel, '{{ $message }}');
                }

                window.serverErrors = [];
                @if (isset($errors))
                    @if (count($errors))
                        window.serverErrors = @json($errors->getMessages());
                    @endif
                @endif

                window._translations = @json(app('Webkul\Velocity\Helpers\Helper')->jsonTranslations());
            })();
        </script>

        <script
            type="text/javascript"
            src="{{ asset('vendor/webkul/ui/assets/js/ui.js') }}">
        </script>

        @stack('scripts')

        <script>
            {!! core()->getConfigData('general.content.custom_scripts.custom_javascript') !!}
        </script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js" crossorigin="anonymous"></script>
        <script type="text/javascript">
            $(document).ready(function(){

              $('#element').tooltip('toggle')

              $.ajaxSetup({
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
              });

              $.ajax({
                type: "post",
                url: "/category-list",
                dataType: "json",
                success: function(data){
                  console.log(data);

                  $.each(data.categories, function(){
                    var category_id = this.id;
                    var icon_path = this.category_icon_path;
                    var position = this.position;
                    if (position == 1) {
                      $.each(data.category_details, function(){
                        if (this.category_id == category_id) {
                          $(".category_list").append('<div class="category_item"><img src="{{asset("storage")}}/'+icon_path+'" alt=""><a href="{{url('/')}}/'+this.url_path+'">'+this.name+'</a></div>');
                        }
                      });
                    }
                  });
                },
                complete: function(){
                  categorySlider();
                },
              });

              function categorySlider(){
                $('.category_list').slick({
                  infinite: true,
                  arrows: true,
                  slidesToShow: 5,
                  slidesToScroll: 1,
                  responsive: [
                        {
                            breakpoint: 1500,
                            settings: {
                                slidesToShow: 4,
                                slidesToScroll: 1,
                            }
                        },
                        {
                            breakpoint: 1300,
                            settings: {
                                slidesToShow: 3,
                                slidesToScroll: 1,
                            }
                        },
                        {
                            breakpoint: 900,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 1
                            }
                        },
                        {
                            breakpoint: 600,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1
                            }
                        },
                        {
                            breakpoint: 480,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1
                            }
                        }

                  ]
                });
              }
            });

        </script>
    </body>
</html>
