<style media="screen">

.footer-content .row {
  padding:          2rem 4rem!important;
  background-image: linear-gradient(90deg, rgba(255, 133, 0, 1) 10%, rgba(255, 145, 25, 1) 20%, rgba(255, 157, 51, 1) 30%)!important;
  display: flex;
  align-items: center;
}

.footer-content a{
  font-size:      1.5em;
  color:          #2f2f2f;
  margin:         1em;
  text-transform: uppercase;
  border:         1px solid #f2f2f2;
  border-radius:  100%;
  background:     #f2f2f2;
  width:          1.5em;
  display:        inline-block;
  text-align:     center;
}

.footer-content span{
  font-size: 1.5em;
  color:     #f2f2f2;
}
</style>

<div class="footer">
    <div class="footer-content">
      <div class="row">
        <div class="col-9">
          <!-- <a href="{{url('/')}}">Home</a>
          <a href="#">about us</a>
          <a href="#">customer care</a>
          <a href="#">delivery & payment</a>
          <a href="#">contact us</a><br><br><br> -->
          <span>&copy; Copyright &#169; 2020 BuildMart</span>
        </div>

        <div class="col-3 text-right">
          <!-- <a href="/customer/login">sign in</a><br><br><br> -->
          <a href="#">
            <i class="fab fa-facebook-f"></i>
          </a>
          <a href="#">
            <i class="fab fa-twitter"></i>
          </a>
          <a href="#">
            <i class="fab fa-instagram"></i>
          </a>
          <!-- <a href="#">
            <i style="font-size: 25px;" class="fab fa-google-plus-g"></i>
          </a> -->
        </div>
      </div>
        {{-- @include('shop::layouts.footer.newsletter-subscription') --}}
        {{-- @include('shop::layouts.footer.footer-links') --}}

        {{-- @if ($categories)
            @include('shop::layouts.footer.top-brands')
        @endif --}}

        {{-- @if (core()->getConfigData('general.content.footer.footer_toggle'))
            @include('shop::layouts.footer.copy-right')
        @endif --}}
    </div>
</div>
