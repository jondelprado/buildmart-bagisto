<header class="sticky-header" v-if="!isMobile()" style="display:none;">
    <div style="background-image: linear-gradient(90deg, rgba(255, 133, 0, 1) 10%, rgba(255, 145, 25, 1) 20%, rgba(255, 157, 51, 1) 30%);" class="row col-12 remove-padding-margin velocity-divide-page">
      <logo-component add-class="navbar-brand"></logo-component>
      <searchbar-component></searchbar-component>
    </div>
</header>

@push('scripts')
    <script type="text/javascript">
        (() => {
            document.addEventListener('scroll', e => {
                scrollPosition = Math.round(window.scrollY);

                if (scrollPosition > 50){
                    document.querySelector('header').classList.add('header-shadow');
                    document.querySelector('header').style.display = "block";
                } else {
                    document.querySelector('header').classList.remove('header-shadow');
                    document.querySelector('header').style.display = "none";
                }
            });
        })()
    </script>
@endpush
