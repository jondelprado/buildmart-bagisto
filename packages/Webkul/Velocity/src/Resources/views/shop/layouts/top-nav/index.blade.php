<nav id="top">
  <div class="col-sm-3" id="logo_container">
    <img width="100%" height="100" src="{{asset('images/bm_logo_blank.png')}}" alt="">
  </div>
  <div class="col-sm-7 text-center" id="nav_container">
    <div class="row">
      <div class="col-sm menu_item">
        <a href="/">Home</a>
      </div>
      <div class="col-sm menu_item">
        <a href="/">About Us</a>
      </div>
      <div class="col-sm menu_item">
        <a href="#">FAQ's</a>
      </div>
      <div class="col-sm menu_item">
        <a href="#">Track My Order</a>
      </div>
      <div class="col-sm menu_item">
        <a href="#">Contact Us</a>
      </div>
      <div class="col-sm menu_item">
        <a href="#">Login/Sign-up</a>
      </div>
    </div>
  </div>

  <div class="col-sm-2 cart_item" id="cart_container">
    <a href="/checkout/cart"><i class="fas fa-shopping-cart"></i><br>Cart/Check-out</a>
  </div>
  
    {{-- <div class="col-sm-6">
        @include('velocity::layouts.top-nav.locale-currency')
    </div>

    <div class="col-sm-6">
        @include('velocity::layouts.top-nav.login-section')
    </div> --}}
</nav>
